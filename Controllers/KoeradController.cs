﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KolmasMVCkoerad.Models;
using F=System.IO;
using Newtonsoft.Json;


namespace KolmasMVCkoerad.Controllers
{
    public class KoeradController : Controller
    
    

       
    {

        string KoeradFile;
        public void LoeKoerad()
        {

        KoeradFile = Server.MapPath("~/App_Data/koerad.json");
        JsonConvert.DeserializeObject<List<Koer>>(F.File.ReadAllText(KoeradFile))
                .ForEach(x => x.Add());

        }
        public void SalvestaKoerad()
        {
            KoeradFile = Server.MapPath("~/App_Data/koerad.json");
            F.File.WriteAllText(KoeradFile, JsonConvert.SerializeObject(Koer.Koerad));
        }
       
      
        
        //;
       
            
            
            
        // GET: Koerad
        public ActionResult Index()
        {
            LoeKoerad();
            return View(Koer.Koerad);
        }

        // GET: Koerad/Details/5
        public ActionResult Details(int id)
        {
            return View(Koer.Find(id));
        }

        // GET: Koerad/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Koerad/Create
        [HttpPost]
        public ActionResult Create(Koer koer)
        {
            try
            {
                koer.Add();
                SalvestaKoerad();
                // TODO: Add insert logic here
                

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Koerad/Edit/5
        public ActionResult Edit(int id)
        {
            return View(Koer.Find(id));
        }

        // POST: Koerad/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Koer uusKoer)
        {

            try
            {

                Koer vanaKoer = Koer.Find(id);
                if(vanaKoer != null)
                {
                    vanaKoer.Nimi = uusKoer.Nimi;
                    vanaKoer.Toug = uusKoer.Toug;
                    vanaKoer.Varv = uusKoer.Varv;

                }
                SalvestaKoerad();
                // TODO: Add update logic here
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Koerad/Delete/5
        public ActionResult Delete(int id)
        {
            
            return View(Koer.Find(id));
        }

        // POST: Koerad/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                Koer.Find(id)?.Remove();
                SalvestaKoerad();
               

                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
