﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KolmasMVCkoerad.Models
{
    public class Koer
    {
        static Dictionary<int, Koer> _Koerad = new Dictionary<int, Koer>();
       static int nr = 0;
        public string Nimi { get; set; }
        public string Toug { get; set; }
        public string Varv { get; set; }
        public int Id { get; set; } = ++nr;
        
        
        public static IEnumerable<Koer> Koerad => _Koerad.Values;
        public void Add()
        {
            if (!_Koerad.ContainsKey(this.Id))
                _Koerad.Add(this.Id, this);
            nr = _Koerad.Keys.Max();
        }
        public static Koer Find(int id) => _Koerad.ContainsKey(id) ? _Koerad[id] : null;              //.Where(x => x.Id == id).SingleOrDefault();
        public void Remove() => _Koerad.Remove(this.Id);
        public static void Remove(int id) => _Koerad.Remove(id);    //overload ehk kaks meetodit sama nimega, aga ühel on parameeter ka



    }
}